#!/usr/bin/env ruby

expected_output   = open('expected-output.txt').read
expected_contents = open('expected-contents.txt').read

#actual_output = `cat test.ed | ed test.txt`
#actual_output = `cat test.ed | ruby -I ../../loled/lib ../../loled/exe/loled test.txt`
actual_output = `cat test.ed | ruby ../golfed.rb test.txt`


actual_contents = open('test.txt').read

if actual_output != expected_output
  puts "Actual output didn't match expected output. Actual output:"
  puts
  puts actual_output.inspect
  puts
  puts "Expected output:"
  puts expected_output.inspect
  puts
  puts
end

if actual_contents != expected_contents
  puts "Actual contents didn't match expcted contents. Actual contents:"
  puts actual_contents.inspect
  puts
  puts "Expected contents:"
  puts expected_contents.inspect
end
