#!/usr/bin/env ruby
require'rbsed'
ls=open(f=$*[0],?r).read.split(?\n,-1)
$i=$stdin
l=0
b=nil
m=!1
rc=->{
t=[]
l=nil
loop{l=$i.gets.chomp
l==?.&&break
t<< l.to_s}
t}
cs={i:->(a,b,_){m=1;ls[a..a]=[*ls[a],*rc.()]},
a:->(a,b,_){m=1;ls[b..b]=[ls[b],*rc.()]},
d:->(a,b,_){m=1;ls[a..b]=[]},
n:->(a,b,_){puts ls[a..b].each_with_index.map{|l,i|(i+1).to_s.ljust(8)+l}},
p:->(a,b,_){puts ls[a..b]},
s:->(a,b,e){ls[a..b]=ls[a..b].map{|o|n=o.sed(?s+e);m||=o!=n;n}},
w:->(*_){open(f,?w){|f|f.puts ls*?\n};m=!1},
q:->(*_){m&&b!=:q ?(puts ??):exit}}
s=->(s){case s
when'.';l
when'$';ls.length-1
when'+';l+1
when'-','^';l-1
when /[+-]\d+/;l+Integer(s)
else Integer(s)-1
end}
r=->(a){a||return
a='1,$'if a[0]==?,||a.include?(',,')||a.include?(?%)
a='.,$'if a.include?(?;)
x=a.split(?,).map(&s)
x[1]||x=[x[0]]*2
x}
loop{t=$i.gets.to_s.chomp
ad,c,a=t.match(/^([^a-zA-Z]+)?([a-zA-Z]+)?(.*)$/).captures
ad=r.(ad)
c&&c=:"#{c}"
if t==?.
puts ls[l]
elsif !c&&ad
puts ls[l=ad[-1]]
elsif cs[c]
cs[c].(*ad||[l,l],a)
b=c
else$stderr.puts'?'end}
