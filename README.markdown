# golfed

Codegolfed partial ed clone.

Supported commands:

* a: Append
* i: Insert
* d: Delete
* n: Print, with line numbers.
* p: Print, without line numbers.
* s: Replace text.
* w: Write to disk.
* q: Quit.
